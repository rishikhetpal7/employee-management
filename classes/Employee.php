<?php

require_once __DIR__."/../helper/requirements.php";

class Employee{
    private $table = "employees";
    private $tables_required = [
        "address" => "address",
        "address_employee" => "address_employee"
    ];
    private $database;
    protected $di;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    
    private function validateData($data)
    {
        $rules = [
            

        ];
        $validator = $this->di->get('validator');
        // return true;
        return $validator->check($data, $rules);
    }
    /**
     * This function is responsible to accept the data from the Routing and add it to the Database.
     */
    public function addEmployee($data)
    {
        $validation = $this->validateData($data);
        if(!$validation->fails())
        {
            //Validation was successful
            try
            {
                //Begin Transaction
                $this->database->beginTransaction();
                $data_to_be_inserted = ['first_name' => $data['first_name'], 'last_name' => $data['last_name'],'gender' => $data['gender']];
                $employee_id = $this->database->insert($this->table, $data_to_be_inserted);
                $address_id = $this->di->get('address')->addAddress($data, $data['number_of_address'], $employee_id);
                $data_for_phone = ['employee_id' => $employee_id, 'phone_number' => $data['phone_no'], 'primary_no'=>1];
                $data_for_whatsapp = ['employee_id' => $employee_id, 'phone_number' => $data['phone_no_whatsapp'], 'isWhatsapp' => 1, 'primary_no'=>1];
                $data_for_email = ['employee_id' => $employee_id, 'email' => $data['email_id'], 'primary_mail' => 1];
                $result = $this->database->insert('contact_email', $data_for_email);
                $result = $this->database->insert('contact_phone', $data_for_phone);
                $result = $this->database->insert('contact_phone', $data_for_whatsapp);

                $patternForPhone = "/phone_no_[0-9]+/";
                foreach($data as $key => $value) {
                    if (preg_match($patternForPhone,$key)){
                        $result = $this->database->insert('contact_phone', ['employee_id' => $employee_id, 'phone_number' => $value]);
                    }
                }

                $patternForWhatsapp = "/phone_no_whatsapp_[0-9]+/";
                foreach($data as $key => $value) {
                    if (preg_match($patternForWhatsapp,$key)){
                        $result = $this->database->insert('contact_phone', ['employee_id' => $employee_id, 'phone_number' => $value,  'isWhatsapp' => 1]);
                    }
                }

                $patternForEmail = "/email_id_[0-9]+/";
                foreach($data as $key => $value) {
                    if (preg_match($patternForEmail,$key)){
                        $result = $this->database->insert('contact_email', ['employee_id' => $employee_id, 'email' => $value]);
                    }
                }
                // Util::dd();
                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch(Exception $e)
            {
                $this->database->rollback();
                return ADD_ERROR;
            }
        }
        else
        {
            //Validation Failed!
            return VALIDATION_ERROR;
        }
    }

    public function getJSONDataForDataTable($draw, $searchParameter, $orderBy, $start, $length){
        $columns = ["last_name", "phone_no", "email_id", "gender"];
        $totalRowCountQuery = "SELECT COUNT(id) as total_count FROM {$this->table} WHERE deleted = 0";
        $filteredRowCountQuery = "SELECT COUNT(id) as filtered_total_count FROM {$this->table} WHERE deleted = 0";
        $query = "SELECT employees.id AS employee, CONCAT(first_name ,' ', last_name) AS last_name, contact_phone.phone_number AS phone_no,contact_email.email AS email_id, gender, CONCAT(address.block_no,' ', address.street, ', ', address.city, ', ',address.pincode, ', ', address.state, ', ', address.country) AS address_data, address.id AS address FROM employees JOIN address JOIN address_employee JOIN contact_phone JOIN contact_email ON employees.id = address_employee.employee_id AND address.id = address_employee.address_id AND contact_phone.employee_id = employees.id AND contact_email.employee_id =employees.id WHERE employees.deleted = 0 and contact_phone.isWhatsapp = 0 AND contact_phone.primary_no = 1 AND address_employee.primary_add = 1 AND contact_email.primary_mail = 1
        ";
        

        if($searchParameter != null){
            $query .= " AND CONCAT(first_name, last_name) like '%{$searchParameter}%'";
            $filteredRowCountQuery .= " AND CONCAT(first_name, last_name) like '%{$searchParameter}%'";
            // Util::dd($filteredRowCountQuery);
        }
        if ($orderBy != null) {
            $query .= " ORDER BY {$columns[$orderBy[0]['column']]} {$orderBy[0]['dir']}";
        }
        else{
            $query .= " ORDER BY {$columns[0]} ASC";
        }

        if($length != -1){
            $query .= " LIMIT {$start}, {$length}";
        }

        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;

        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->filtered_total_count : 0;

        // Util::dd($query);
        $filteredData = $this->database->raw($query);
        $numberOfRowsToDisplay = is_array($filteredData) ? count($filteredData) : 0;
        $data = [];

        for($i = 0; $i < $numberOfRowsToDisplay; $i++){
            $subarray = [];
            // $subarray[] = $i+1;
            $subarray[] = $filteredData[$i]->last_name;
            $subarray[] = $filteredData[$i]->phone_no;
            $subarray[] = $filteredData[$i]->email_id;
            $subarray[] = $filteredData[$i]->gender;
            $subarray[] = $filteredData[$i]->address_data;
            $subarray[] = <<<BUTTONS
            <button class="view btn btn-outline-dark" id="{$filteredData[$i]->employee}" data-toggle = "modal" data-target = "#editModal">
                <i class="fa fa-eye"></i>
            </button>
            <button class="edit btn btn-outline-primary" id="{$filteredData[$i]->employee}" data-toggle = "modal" data-target = "#editModal">
                <i class="fas fa-pencil-alt"></i>
            </button>
            <button class="delete btn btn-outline-danger" id="{$filteredData[$i]->employee}" data-toggle = "modal" data-target = "#deleteModal">
                <i class="fas fa-trash"></i>
            </button>            
BUTTONS;
            $data[] = $subarray;
        }
        // print_r($data);
        // echo "<br>";
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $numberOfTotalRows,
            "recordsFiltered" => $numberOfFilteredRows,
            "data" => $data
        );
        // Util::dd($output);
        // var_dump($query);
        // var_dump($filteredRowCountQuery);
        // var_dump($totalRowCountQuery);
        echo json_encode($output);
    }

    public function getEmployeeById($category_id, $mode=PDO::FETCH_OBJ){
        $query = "SELECT id AS employee_id, first_name, last_name, gender FROM {$this->table} WHERE deleted = 0 AND id = {$category_id}";
        $result = $this->database->raw($query, $mode);
        return $result;
    }

    public function getEmail($category_id, $mode=PDO::FETCH_OBJ){
        $query = "SELECT id AS email_id, email, primary_mail FROM contact_email WHERE deleted = 0 AND employee_id = {$category_id}";
        $result = $this->database->raw($query, $mode);
        return $result;
    }

    public function getPhoneNumber($category_id, $Whatsapp = false, $mode=PDO::FETCH_ASSOC){
        
        if($Whatsapp){
            $query = "SELECT contact_phone.id AS phone_whatsapp_id, contact_phone.phone_number AS phone_no, contact_phone.primary_no AS primary_no FROM contact_phone WHERE contact_phone.employee_id = {$category_id}  AND  contact_phone.isWhatsapp = 1 AND contact_phone.deleted = 0";
        }else{
            $query = "SELECT contact_phone.id AS phone_no_id, contact_phone.phone_number AS phone_no, contact_phone.primary_no AS primary_no FROM contact_phone WHERE contact_phone.employee_id = {$category_id}  AND  contact_phone.isWhatsapp = 0 AND contact_phone.deleted = 0";
        }
        $result = $this->database->raw($query, $mode);
        return $result;
    }

    public function deleteNumber($employee_id,$number_id,  $Whatsapp = false){
        if($Whatsapp){
            $query = "SELECT contact_phone.id AS phone_whatsapp_id, contact_phone.phone_number AS phone_no, contact_phone.primary_no AS primary_no FROM contact_phone WHERE contact_phone.employee_id = {$employee_id}  AND  contact_phone.isWhatsapp = 1 AND contact_phone.deleted = 0 AND contact_phone.id = {$number_id}";
        }else{
            $query = "SELECT contact_phone.id AS phone_no_id, contact_phone.phone_number AS phone_no, contact_phone.primary_no AS primary_no FROM contact_phone WHERE contact_phone.employee_id = {$employee_id}  AND  contact_phone.isWhatsapp = 0 AND contact_phone.deleted = 0 AND contact_phone.id = {$number_id}";
        }
        $result = $this->database->raw($query);
        if(count($result) > 0){
            $this->database->delete("contact_phone", "contact_phone.id = $number_id");
            return DELETE_SUCCESS;;
        }else{
            return DELETE_ERROR;
        }

    }
    public function deleteEmail($employee_id,$number_id){
        
        $query = "SELECT id FROM contact_email WHERE contact_email.employee_id = {$employee_id} AND contact_email.deleted = 0 AND contact_email.id = {$number_id} AND contact_email.primary_mail = 0";
        $result = $this->database->raw($query);
        if(count($result) > 0){
            $this->database->delete("contact_email", "contact_email.id = $number_id");
            return DELETE_SUCCESS;;
        }else{
            return DELETE_ERROR;
        }

    }
    public function getEmployeeWithAddress($category_id, $concat = true, $mode = PDO::FETCH_ASSOC){
         
        if($concat)
            $query = "SELECT employees.id, employees.first_name, employees.last_name, employees.gst_no, employees.phone_no, employees.email_id, employees.gender, CONCAT(address.block_no,' ', address.street, ', ', address.city, ', ',address.pincode, ', ', address.state, ', ', address.country), address.id AS address FROM employees JOIN address JOIN address_employee ON employees.id = address_employee.employee_id AND address.id = address_employee.address_id AND employees.id = {$category_id}";
        else
            $query = "SELECT employees.id, employees.first_name, employees.last_name, employees.gst_no, employees.phone_no, employees.email_id, employees.gender, address.block_no, address.street, address.city, address.pincode, address.state,  address.country, address.id AS address_id FROM employees JOIN address JOIN address_employee ON employees.id = address_employee.employee_id AND address.id = address_employee.address_id AND employees.id = {$category_id}";
        $result = $this->database->raw($query);
        return $result;
    }
    public function update($data, $employee_id){

        $validation = $this->validateData($data);
        if(!$validation->fails()) 
        {
            //Validation was successful
            try
            {
                // Util::dd($data);
                //Begin Transaction
                $this->database->beginTransaction();
                $data_to_be_updated = ['first_name' => $data['first_name'], 'last_name' => $data['last_name'],'gender' => $data['gender']];
                $this->database->update($this->table, $data_to_be_updated, "id = {$employee_id}");
                $data_for_phone = ['phone_number' => $data['phone_no']];
                $data_for_whatsapp = ['phone_number' => $data['phone_no_whatsapp']];
                $data_for_email = ['email' => $data['email_id']];
                $result = $this->database->update('contact_email', $data_for_email, "id = {$data['email_id_d_1']}");
                $result = $this->database->update('contact_phone', $data_for_phone, "id = {$data['phone_no_d_1']}");
                $result = $this->database->update('contact_phone', $data_for_whatsapp, "id = {$data['phone_no_whatsapp_d_1']}");

                $patternForPhone = "/phone_no_[0-9]+/";
                foreach($data as $key => $value) {
                    if (preg_match($patternForPhone,$key)){
                        $array_phone = explode("_", $key);
                        $number_id = $data['phone_no_d_'+$array_phone[2]];
                        $query = "SELECT contact_phone.id AS phone_no_id, contact_phone.phone_number AS phone_no, contact_phone.primary_no AS primary_no FROM contact_phone WHERE contact_phone.employee_id = {$employee_id}  AND  contact_phone.isWhatsapp = 0 AND contact_phone.deleted = 0 AND contact_phone.id = {$number_id}";
                        $result = $this->database->raw($query);
                        if(count($result) > 0){
                            $result = $this->database->update('contact_email', ['phone_number' => $value], "id = {$number_id}");
                        }else{
                            $result = $this->database->insert('contact_phone', ['employee_id' => $employee_id, 'phone_number' => $value]);    
                        }
                        
                    }
                }

                $patternForWhatsapp = "/phone_no_whatsapp_[0-9]+/";
                foreach($data as $key => $value) {
                    if (preg_match($patternForWhatsapp,$key)){
                        $array_phone = explode("_", $key);
                        $number_id = $data['phone_no_whatsapp_d_'+$array_phone[3]];
                        $query = "SELECT contact_phone.id AS phone_whatsapp_id, contact_phone.phone_number AS phone_no, contact_phone.primary_no AS primary_no FROM contact_phone WHERE contact_phone.employee_id = {$employee_id}  AND  contact_phone.isWhatsapp = 1 AND contact_phone.deleted = 0 AND contact_phone.id = {$number_id}";
                        $result = $this->database->raw($query);
                        if(count($result) > 0){
                            $result = $this->database->update('contact_phone', ['phone_number' => $value], "id = {$number_id}");
                        }else{
                            $result = $this->database->insert('contact_phone', ['employee_id' => $employee_id, 'phone_number' => $value]);    
                        }
                    }
                }

                $patternForEmail = "/email_id_[0-9]+/";
                foreach($data as $key => $value) {
                    if (preg_match($patternForEmail,$key)){
                        $array_phone = explode("_", $key);
                        $number_id = $data['email_id_d_'+$array_phone[2]];
                        $query = "SELECT id FROM contact_email WHERE contact_email.employee_id = {$employee_id} AND contact_email.deleted = 0 AND contact_email.id = {$number_id} AND contact_email.primary_mail = 0";
                        $result = $this->database->raw($query);
                        if(count($result) > 0){
                            $result = $this->database->update('contact_email', ['email' => $value], "id = {$number_id}");
                        }else{
                            $result = $this->database->insert('contact_email', ['employee_id' => $employee_id, 'email' => $value]);    
                        }
                    }
                }
                $patternForEmail = "/email_id_[0-9]+/";
                foreach($data as $key => $value) {
                    if (preg_match($patternForEmail,$key)){
                        $array_phone = explode("_", $key);
                        
                        $number_id = $data['email_id_d_'+$array_phone[2]];
                        $query = "SELECT id FROM contact_email WHERE contact_email.employee_id = {$employee_id} AND contact_email.deleted = 0 AND contact_email.id = {$number_id} AND contact_email.primary_mail = 0";
                        $result = $this->database->raw($query);
                        if(count($result) > 0){
                            $result = $this->database->update('contact_email', ['email' => $value], "id = {$number_id}");
                        }else{
                            $result = $this->database->insert('contact_email', ['employee_id' => $employee_id, 'email' => $value]);    
                        }
                    }
                }
                $patternForEmail = "/address_d_0/";
                foreach($data as $key => $value) {
                    if (preg_match($patternForEmail,$key)){
                        $array_phone = explode("_", $key);
                        $number_id = $data['address_d_0'];
                        // Util::dd($number_id);
                        $address_to_be_updated = [
                            "block_no" => $data["block_no_0"],
                            "street" => $data["street_0"],
                            "city" => $data["city_0"],
                            "pincode" => $data["pincode_0"],
                            "state" => $data["state_0"],
                            "country" => $data["country_0"],
                            "town" => $data["town_0"]
                        ];
                        if($number_id == 0){
                            $result = $this->database->insert('address', $address_to_be_updated);    
                            $result = $this->database->insert('address_employee', [
                                'address_id'=> $result,
                                'employee_id'=>$employee_id
                            ]);
                        }else{
                            if($this->di->get('address')->updateAddress($address_to_be_updated, $number_id) == EDIT_ERROR){
                                throw new Exception();
                            }
                        }
                    }
                }
                $patternForEmail = "/address_d_1/";
                foreach($data as $key => $value) {
                    if (preg_match($patternForEmail,$key)){
                        $array_phone = explode("_", $key);
                        $number_id = $data['address_d_1'];
                        // Util::dd($number_id);
                        $address_to_be_updated = [
                            "block_no" => $data["block_no_1"],
                            "street" => $data["street_1"],
                            "city" => $data["city_1"],
                            "pincode" => $data["pincode_1"],
                            "state" => $data["state_1"],
                            "country" => $data["country_1"],
                            "town" => $data["town_1"]
                        ];
                        if($number_id == 0){
                            $result = $this->database->insert('address', $address_to_be_updated);    
                            $result = $this->database->insert('address_employee', [
                                'address_id'=> $result,
                                'employee_id'=>$employee_id
                            ]);
                        }else{
                            // Util::dd("Update");
                            if($this->di->get('address')->updateAddress($address_to_be_updated, $number_id) == EDIT_ERROR){
                                throw new Exception();
                            }
                        }
                    }
                }
                $this->database->commit();
                return EDIT_SUCCESS;
            }
            catch(Exception $e)
            {
                $this->database->rollback();
                return EDIT_ERROR;
            }
        }
        else
        {
            //Validation Failed!
            return VALIDATION_ERROR;
        }
    }

    public function delete($employee_id)
    {
        try{
           $this->database->beginTransaction();
           $this->database->delete($this->table, "id={$employee_id}");
        //    $this->di->get('address')->delete($address_id);
           $this->database->commit(); 
           return DELETE_SUCCESS;
        } catch (Exception $e) {
            $this->database->rollback();
            return DELETE_ERROR;
        }
    }
}