<?php

require_once __DIR__."/../helper/requirements.php";

class Address{
    private $table = "address";
    
    private $database;
    protected $di;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    
    private function validateData($data)
    {
        $validator = $this->di->get('validator');
        return $validator->check($data, [
            
         
        ]);
    }
    /**
     * This function is responsible to accept the data from the Routing and add it to the Database.
     */
    public function addAddress($data, $number, $owner_id)
    {
        $validation = $this->validateData($data);
        if(!$validation->fails())
        {
            //Validation was successful
            try
            {
                //Begin Transaction
                for($i = 0; $i < $number; $i++){

                    $address_to_be_inserted = [
                        "block_no" => $data["block_no_$i"],
                        "street" => $data["street_$i"],
                        "city" => $data["city_$i"],
                        "pincode" => $data["pincode_$i"],
                        "state" => $data["state_$i"],
                        "country" => $data["country_$i"],
                        "town" => $data["town_$i"]
                    ];
                    if($i ==0){
                        $address_to_be_inserted['primary_add'] = "1";
                    }
                    $address_id = $this->database->insert($this->table, $address_to_be_inserted);
                    $result = $this->database->insert('address_employee', [
                        'address_id'=> $address_id,
                        'employee_id'=>$owner_id
                    ]);
                }
                 
                return $address_id;
            }
            catch(Exception $e)
            {
                return ADD_ERROR;
            }
        }
        else
        {
            //Validation Failed!
            return VALIDATION_ERROR;
        }
    }


    public function getAddressById($owner_id, $mode=PDO::FETCH_OBJ){
        $query = "SELECT address.id AS address_id, block_no, street, city, pincode, state, country, town, primary_add FROM employees JOIN address JOIN address_employee ON employees.id = address_employee.employee_id AND address.id = address_employee.address_id WHERE employees.deleted = 0 AND address_employee.employee_id = $owner_id";
        $result = $this->database->raw($query, $mode);
        return $result;
    }

    public function updateAddress($data, $id){

        $validation = $this->validateData($data);
        if(!$validation->fails()) 
        {
            //Validation was successful
            try
            {
                //Begin Transaction
                $address_to_be_updated = [
                    'block_no' => $data['block_no'],
                    'street' => $data['street'],
                    'city' => $data['city'],
                    'pincode' => $data['pincode'],
                    'state' => $data['state'],
                    'country' => $data['country'],
                    'town' => $data['town']
                ];
                $this->database->update($this->table, $address_to_be_updated, "id = {$id}");
                
                return EDIT_SUCCESS;
            }
            catch(Exception $e)
            {
                return EDIT_ERROR;
            }
        }
        else
        {
            //Validation Failed!
            return VALIDATION_ERROR;
        }
    }

    public function delete($id)
    {
        try{
           
           $this->database->delete($this->table, "id={$id}");
           
           return DELETE_SUCCESS;
        } catch (Exception $e) {
           
            return DELETE_ERROR;
        }
    }
}