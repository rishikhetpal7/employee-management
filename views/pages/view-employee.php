<?php
require_once __DIR__ . '/../../helper/init.php';
$pageTitle = "Easy ERP | View Employee";

if(isset($_GET['id'])){

  $employee_id = $_GET['id'];
}else{
  $index = BASEPAGES +"index.php";
  header("location: $index");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php  require_once __DIR__ . "/../includes/head-section.php"; ?>
  <!--PLACE TO ADD YOUR CUSTOM CSS-->
  <link rel="stylesheet" href="<?=BASEASSETS;?>vendor/toastr/toastr.min.css">
  <link href="<?= BASEASSETS; ?>vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php require_once(__DIR__. "/../includes/navbar.php");?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Employee</h1>
            <a href="<?= BASEPAGES;?>add-employee.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fa fa-pencil-alt fa-sm text-white-75"></i> Edit Employee
            </a>
          </div>
          <div class="row">
            <div class="col-lg-12">

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Employee Details</h6>
              </div>
              <div class="card-body">
                <div class="col-md-12">
                  
                    <!-- FORM GROUP -->
                    <div class="form-group">
                      <div class="row">
                      <div class="col-md-6 col-sm-6">
                      <label for="first_name">Employee First Name</label>
                      <input type="text" name="first_name" id="first_name" class="form-control"
                      value="<?= $result[0]['first_name']?>"/>
                      
                      <br>

                      <label for="gender">Employee Gender</label>
                      <select name="gender" id="gender" class="form-control">
                      <option></option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                      </select>
                     
                      <br>


                    

                      

                      </div>
                      <div class="col-md-6">
                      <label for="last_name">Employee Last Name</label>
                      <input type="text" name="last_name" id="last_name" class="form-control"
                      value="<?= $result[0]['last_name']?>"/>
                     
                      
                      <br>



                      </div>
                      </div>
                      <div class="row">
                        <div id="phone" class="col-md-6 col-sm-6"><input type="hidden" id="phone_no_d_1" name="phone_no_d_1">
                            <label for="phone_no">Employee Phone Number</label> 
                        <input type="number" name="phone_no" id="phone_no" class="form-control"
                        placeholder="Enter Your Phone Number"/>
                        
                        <br>
                        </div>
                      </div>
                      <div class="row">
                        <div id="whatsapp" class="col-md-6 col-sm-6">
                            <label for="phone_no_whatsapp">Employee Whatsapp Number</label><input type="hidden" id="phone_no_whatsapp_d_1" name="phone_no_whatsapp_d_1">
                        <input type="number" name="phone_no_whatsapp" id="phone_no_whatsapp" class="form-control <?= $errors!='' && $errors->has('phone_no_whatsapp')? 'error': '';?>"
                        placeholder="Enter Your Whatsapp Number"/>
                        
                        <br>
                        </div>
                      </div>
                      <div class="row">
                        <div id="email" class="col-md-6 col-sm-6">
                        <label for="email_id">Employee Email ID</label><input type="hidden" id="email_id_d_1" name="email_id_d_1">
                      <input type="email" name="email_id" id="email_id" class="form-control <?= $errors!='' && $errors->has('email_id')? 'error': '';?>"
                      placeholder="Enter Your Email Id"/>
                      
                      
                      <br>
                        </div>
                      </div>

                      <div id="address">
                      
                      <label for="address">Address</label>
                      <input type="hidden" id="number_of_address" name="number_of_address" value="1">
                      <div class="row">
                      <div class="col-md-6">
                      <input type="hidden" id="address_d_0" name="address_d_0">
                      <input type="text" name="block_no_0" id="block_no_0" class="form-control <?= $errors!='' && $errors->has('block_no_0')? 'error': '';?>"
                      placeholder="Enter Block No."/>
                     
                     
                      </div>
                      <div class="col-md-6">
                      <input type="text" name="street_0" id="street_0" class="form-control <?= $errors!='' && $errors->has('street_0')? 'error': '';?>"
                      placeholder="Enter street_0"/>
                     
                      
                      </div>
                      </div>

                      <div class="row pt-3">
                            <div class="col-md-4">
                            <input type="text" name="city_0" id="city_0" class="form-control <?= $errors!='' && $errors->has('city_0')? 'error': '';?>"
                            placeholder="Enter City"/>
                          
                            
                            </div> 

                            <div class="col-md-4">
                            <input type="text" name="state_0" id="state_0" class="form-control <?= $errors!='' && $errors->has('state_0')? 'error': '';?>"
                            placeholder="Enter State"/>
                          
                            
                            </div>

                            <div class="col-md-4">
                            <input type="text" name="town_0" id="town_0" class="form-control <?= $errors!='' && $errors->has('town_0')? 'error': '';?>"
                            placeholder="Enter Town"/>
                          
                           
                            </div> 
                       </div>

                       <div class="row pt-3 pb-3">
                            <div class="col-md-6">
                            <input type="number" name="pincode_0" id="pincode_0" class="form-control <?= $errors!='' && $errors->has('pincode_0')? 'error': '';?>"
                            placeholder="Enter Pin Code"/>
                          
                            
                            </div> 

                            <div class="col-md-6">
                            <input type="text" name="country_0" id="country_0" class="form-control <?= $errors!='' && $errors->has('country_0')? 'error': '';?>"
                            placeholder="Enter Country"/>
                          
                            
                            </div> 
                       </div>
                      </div>
                    <!--/FORM GROUP-->
                    
                  </form>
                </div>
        </div>
        
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      

      <!-- Footer -->
      <?php require_once(__DIR__. "/../includes/footer.php");?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->


  <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
  <?php require_once(__DIR__."/../includes/core-scripts.php");?>
  <!--PAGE LEVEL SCRIPTS-->
  <?php require_once(__DIR__."/../includes/page-level/employee/view-employee-scripts.php");?>


</body>

</html>
