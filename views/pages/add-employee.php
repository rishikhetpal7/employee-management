<?php
require_once __DIR__.'/../../helper/init.php';
$pageTitle = "Easy EM | Add Employee";
$sidebarSection = "employee";
$sidebarSubSection = "addCus";
Util::createCSRFToken();
$errors = "";
if(Session::hasSession('errors'))
{
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}
$old = "";
if(Session::hasSession('old'))
{
    $old = Session::getSession('old');
    Session::unsetSession('old');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require_once __DIR__ . "/../includes/head-section.php";
  ?>

  <!--PLACE TO ADD YOUR CUSTOM CSS-->

</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php require_once(__DIR__ . "/../includes/sidebar.php"); ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php require_once(__DIR__ . "/../includes/navbar.php"); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Employee</h1>
            <a href="<?= BASEPAGES; ?>manage-employee.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fa fa-list-ul fa-sm text-white-75"></i> Manage Employee
            </a>
          </div>

          <div class="row">
            <div class="col-lg-12">

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add Employee</h6>
              </div>
              <div class="card-body">
                <div class="col-md-12">
                  <form action="<?=BASEURL;?>helper/routing.php" method="POST" id="add-employee">
                    <input type="hidden" name="csrf_token" value="<?=Session::getSession('csrf_token');?>">
                    <!-- FORM GROUP -->
                    <div class="form-group">
                      <div class="row">
                      <div class="col-md-6 col-sm-6">
                      <label for="first_name">Employee First Name</label>
                      <input type="text" name="first_name" id="first_name" class="form-control <?= $errors!='' && $errors->has('first_name')? 'error': '';?>"
                      placeholder="Enter Your First Name"/>
                      
                      <?php
                        if($errors!="" && $errors->has('first_name'))
                        {
                          echo "<span class='error'>{$errors->first('first_name')}</span>";
                        }
                      ?>
                      <br>

                      <label for="gender">Employee Gender</label>
                      <select name="gender" id="gender" class="form-control">
                      <option></option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                      </select>
                      <?php
                        if($errors!="" && $errors->has('gender'))
                        {
                          echo "<span class='error'>{$errors->first('gender')}</span>";
                        }
                      ?>
                      <br>


                    

                      

                      </div>
                      <div class="col-md-6">
                      <label for="last_name">Employee Last Name</label>
                      <input type="text" name="last_name" id="last_name" class="form-control <?= $errors!='' && $errors->has('last_name')? 'error': '';?>"
                      placeholder="Enter Your Last Name"/>
                     
                      <?php
                        if($errors!="" && $errors->has('last_name'))
                        {
                          echo "<span class='error'>{$errors->first('last_name')}</span>";
                        }
                      ?>
                      <br>



                      </div>
                      </div>
                      <div class="row">
                        <div id="phone" class="col-md-6 col-sm-6">
                            <label for="phone_no">Employee Phone Number</label> <button type="button" class="btn btn-outline-primary" onclick="addPhone()">+</button>
                        <input type="number" name="phone_no" id="phone_no" class="form-control <?= $errors!='' && $errors->has('phone_no')? 'error': '';?>"
                        placeholder="Enter Your Phone Number"/>
                        <?php
                            if($errors!="" && $errors->has('phone_no'))
                            {
                            echo "<span class='error'>{$errors->first('phone_no')}</span>";
                            }
                        ?>
                        <br>
                        </div>
                      </div>
                      <div class="row">
                        <div id="whatsapp" class="col-md-6 col-sm-6">
                            <label for="phone_no_whatsapp">Employee Whatsapp Number</label><button type="button" class="btn btn-outline-primary" onclick="addWhatsapp()">+</button>
                        <input type="number" name="phone_no_whatsapp" id="phone_no_whatsapp" class="form-control <?= $errors!='' && $errors->has('phone_no_whatsapp')? 'error': '';?>"
                        placeholder="Enter Your Whatsapp Number"/>
                        <?php
                            if($errors!="" && $errors->has('phone_no_whatsapp'))
                            {
                            echo "<span class='error'>{$errors->first('phone_no_whatsapp')}</span>";
                            }
                        ?>
                        <br>
                        </div>
                      </div>
                      <div class="row">
                        <div id="email" class="col-md-6 col-sm-6">
                        <label for="email_id">Employee Email ID</label><button type="button" class="btn btn-outline-primary" onclick="addEmail()">+</button>
                      <input type="email" name="email_id" id="email_id" class="form-control <?= $errors!='' && $errors->has('email_id')? 'error': '';?>"
                      placeholder="Enter Your Email Id"/>
                      
                      <?php
                        if($errors!="" && $errors->has('email_id'))
                        {
                          echo "<span class='error'>{$errors->first('email_id')}</span>";
                        }
                      ?>
                      <br>
                        </div>
                      </div>

                      <div id="address">
                      <button type="button" class="btn btn-outline-primary" onclick="addAddress()">+</button>
                      <label for="address">Address</label>
                      <input type="hidden" id="number_of_address" name="number_of_address" value="1">
                      <div class="row">
                      <div class="col-md-6">
                      <input type="text" name="block_no_0" id="block_no_0" class="form-control <?= $errors!='' && $errors->has('block_no_0')? 'error': '';?>"
                      placeholder="Enter Block No."/>
                     
                      <?php
                        if($errors!="" && $errors->has('block_no_0'))
                        {
                          echo "<span class='error'>{$errors->first('block_no_0')}</span>";
                        }
                      ?>
                      </div>
                      <div class="col-md-6">
                      <input type="text" name="street_0" id="street_0" class="form-control <?= $errors!='' && $errors->has('street_0')? 'error': '';?>"
                      placeholder="Enter street_0"/>
                     
                      <?php
                        if($errors!="" && $errors->has('street_0'))
                        {
                          echo "<span class='error'>{$errors->first('street_0')}</span>";
                        }
                      ?>
                      </div>
                      </div>

                      <div class="row pt-3">
                            <div class="col-md-4">
                            <input type="text" name="city_0" id="city_0" class="form-control <?= $errors!='' && $errors->has('city_0')? 'error': '';?>"
                            placeholder="Enter City"/>
                          
                            <?php
                              if($errors!="" && $errors->has('city_0'))
                              {
                                echo "<span class='error'>{$errors->first('city_0')}</span>";
                              }
                            ?>
                            </div> 

                            <div class="col-md-4">
                            <input type="text" name="state_0" id="state_0" class="form-control <?= $errors!='' && $errors->has('state_0')? 'error': '';?>"
                            placeholder="Enter State"/>
                          
                            <?php
                              if($errors!="" && $errors->has('state_0'))
                              {
                                echo "<span class='error'>{$errors->first('state_0')}</span>";
                              }
                            ?>
                            </div>

                            <div class="col-md-4">
                            <input type="text" name="town_0" id="town_0" class="form-control <?= $errors!='' && $errors->has('town_0')? 'error': '';?>"
                            placeholder="Enter Town"/>
                          
                            <?php
                              if($errors!="" && $errors->has('town_0'))
                              {
                                echo "<span class='error'>{$errors->first('town_0')}</span>";
                              }
                            ?>
                            </div> 
                       </div>

                       <div class="row pt-3 pb-3">
                            <div class="col-md-6">
                            <input type="number" name="pincode_0" id="pincode_0" class="form-control <?= $errors!='' && $errors->has('pincode_0')? 'error': '';?>"
                            placeholder="Enter Pin Code"/>
                          
                            <?php
                              if($errors!="" && $errors->has('pincode_0'))
                              {
                                echo "<span class='error'>{$errors->first('pincode_0')}</span>";
                              }
                            ?>
                            </div> 

                            <div class="col-md-6">
                            <input type="text" name="country_0" id="country_0" class="form-control <?= $errors!='' && $errors->has('country_0')? 'error': '';?>"
                            placeholder="Enter Country"/>
                          
                            <?php
                              if($errors!="" && $errors->has('country_0'))
                              {
                                echo "<span class='error'>{$errors->first('country_0')}</span>";
                              }
                            ?>
                            </div> 
                       </div>
                      </div>
                    <!--/FORM GROUP-->
                    <button type="submit" class="btn btn-primary" name="add_employee" value="addEmployee"><i class="fa fa-check"></i> Submit</button>
                  </form>
                </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <?php require_once(__DIR__ . "/../includes/footer.php"); ?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
  <?php require_once(__DIR__ . "/../includes/core-scripts.php"); ?>
  
  <!--PAGE LEVEL SCRIPTS-->
  <?php require_once(__DIR__ . "/../includes/page-level/employee/add-employee-scripts.php");?>
</body>

</html>