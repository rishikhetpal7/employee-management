<?php

require_once __DIR__."/../helper/requirements.php";

class Customer{
    private $table = "customers";
    private $tables_required = [
        "address" => "address",
        "address_customer" => "address_customer"
    ];
    private $database;
    protected $di;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }
    
    private function validateData($data)
    {
        $validator = $this->di->get('validator');
        return $validator->check($data, [
            'first_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
                
            ],

            'last_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255
                
            ],
            'gst_no' => [
                'required' => true
               
               
            ],
            'phone_no' => [
                // 'unique' => true,    
                'required' => true

            ],
            'email_id' => [
                'email' => true,
                'required' => true
            ],
            'gender'=>[
                'required'=>true
            ]
         
        ]);
    }
    /**
     * This function is responsible to accept the data from the Routing and add it to the Database.
     */
    public function addCustomer($data)
    {
        $validation = $this->validateData($data);
        if(!$validation->fails())
        {
            //Validation was successful
            try
            {
                //Begin Transaction
                $this->database->beginTransaction();
                $data_to_be_inserted = ['first_name' => $data['first_name'], 'last_name' => $data['last_name'], 'gst_no' => $data['gst_no'], 'email_id' => $data['email_id'], 'phone_no' => $data['phone_no'], 'gender' => $data['gender']];
                
                $customer_id = $this->database->insert($this->table, $data_to_be_inserted);
                $address_id = $this->di->get('address')->addAddress($data);
                if($address_id == EDIT_ERROR){
                    throw new Exception();
                }
                $result = $this->database->insert('address_customer', [
                    'address_id'=> $address_id,
                    'customer_id'=>$customer_id
                ]);

                $this->database->commit();
                return ADD_SUCCESS;
            }
            catch(Exception $e)
            {
                $this->database->rollback();
                return ADD_ERROR;
            }
        }
        else
        {
            //Validation Failed!
            return VALIDATION_ERROR;
        }
    }

    public function getJSONDataForDataTable($draw, $searchParameter, $orderBy, $start, $length){
        $columns = ["last_name", "phone_no", "email_id", "gender"];
        $totalRowCountQuery = "SELECT COUNT(id) as total_count FROM {$this->table} WHERE deleted = 0";
        $filteredRowCountQuery = "SELECT COUNT(id) as filtered_total_count FROM {$this->table} WHERE deleted = 0";
        $query = "SELECT id, CONCAT(first_name ,' ', last_name) AS last_name, phone_no, email_id, gender FROM {$this->table} WHERE deleted = 0";

        if($searchParameter != null){
            $query .= " AND CONCAT(first_name, last_name) like '%{$searchParameter}%'";
            $filteredRowCountQuery .= " AND CONCAT(first_name, last_name) like '%{$searchParameter}%'";
            // Util::dd($filteredRowCountQuery);
        }
        if ($orderBy != null) {
            $query .= " ORDER BY {$columns[$orderBy[0]['column']]} {$orderBy[0]['dir']}";
        }
        else{
            $query .= " ORDER BY {$columns[0]} ASC";
        }

        if($length != -1){
            $query .= " LIMIT {$start}, {$length}";
        }

        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;

        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->filtered_total_count : 0;

        // Util::dd($query);
        $filteredData = $this->database->raw($query);
        $numberOfRowsToDisplay = is_array($filteredData) ? count($filteredData) : 0;
        $data = [];

        for($i = 0; $i < $numberOfRowsToDisplay; $i++){
            $subarray = [];
            // $subarray[] = $i+1;
            $subarray[] = $filteredData[$i]->last_name;
            $subarray[] = $filteredData[$i]->phone_no;
            $subarray[] = $filteredData[$i]->email_id;
            $subarray[] = $filteredData[$i]->gender;
            $subarray[] = <<<BUTTONS
            <button class="view btn btn-outline-dark" id="{$filteredData[$i]->id}" data-toggle = "modal" data-target = "#editModal">
                <i class="fa fa-eye"></i>
            </button>
            <button class="edit btn btn-outline-primary" id="{$filteredData[$i]->id}" data-toggle = "modal" data-target = "#editModal">
                <i class="fas fa-pencil-alt"></i>
            </button>
            <button class="delete btn btn-outline-danger" id="{$filteredData[$i]->id}" data-toggle = "modal" data-target = "#deleteModal">
                <i class="fas fa-trash"></i>
            </button>            
BUTTONS;
            $data[] = $subarray;
        }
        // print_r($data);
        // echo "<br>";
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $numberOfTotalRows,
            "recordsFiltered" => $numberOfFilteredRows,
            "data" => $data
        );
        // Util::dd($output);
        // var_dump($query);
        // var_dump($filteredRowCountQuery);
        // var_dump($totalRowCountQuery);
        echo json_encode($output);
    }

    public function getCustomerById($category_id, $mode=PDO::FETCH_OBJ){
        $query = "SELECT * FROM {$this->table} WHERE deleted = 0 AND id = {$category_id}";
        $result = $this->database->raw($query, $mode);
        return $result;
    }

    public function getCustomerWithAddress($category_id, $concat = true, $mode = PDO::FETCH_ASSOC){
         
        if($concat)
            $query = "SELECT customers.id, customers.first_name, customers.last_name, customers.gst_no, customers.phone_no, customers.email_id, customers.gender, CONCAT(address.block_no,' ', address.street, ', ', address.city, ', ',address.pincode, ', ', address.state, ', ', address.country), address.id AS address FROM customers JOIN address JOIN address_customer ON customers.id = address_customer.customer_id AND address.id = address_customer.address_id AND customers.id = {$category_id}";
        else
            $query = "SELECT customers.id, customers.first_name, customers.last_name, customers.gst_no, customers.phone_no, customers.email_id, customers.gender, address.block_no, address.street, address.city, address.pincode, address.state,  address.country, address.town,address.pincode, address.id AS address_id FROM customers JOIN address JOIN address_customer ON customers.id = address_customer.customer_id AND address.id = address_customer.address_id AND customers.id = {$category_id}";
        $result = $this->database->raw($query);
        return $result;
    }
    public function update($data, $customer_id, $address_id){

        $validation = $this->validateData($data);
        if(!$validation->fails()) 
        {
            //Validation was successful
            try
            {
                //Begin Transaction
                $this->database->beginTransaction();
                $data_to_be_updated = ['first_name' => $data['first_name'], 'last_name' => $data['last_name'], 'gst_no' => $data['gst_no'], 'email_id' => $data['email_id'], 'phone_no' => $data['phone_no'], 'gender' => $data['gender']];
                $this->database->update($this->table, $data_to_be_updated, "id = {$customer_id}");
                
                if($this->di->get('address')->updateAddress($data, "id = {$customer_id}") == EDIT_ERROR){
                    throw new Exception();
                }
                $this->database->commit();
                return EDIT_SUCCESS;
            }
            catch(Exception $e)
            {
                $this->database->rollback();
                return EDIT_ERROR;
            }
        }
        else
        {
            //Validation Failed!
            return VALIDATION_ERROR;
        }
    }

    public function delete($id)
    {
        try{
           $this->database->beginTransaction();
           $this->database->delete($this->table, "id={$id}");
           $this->database->commit(); 
           return DELETE_SUCCESS;
        } catch (Exception $e) {
            $this->database->rollback();
            return DELETE_ERROR;
        }
    }
}