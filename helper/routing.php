<?php
require_once 'init.php';


if(isset($_POST['delete_employee'])){

    // echo "HEllo";
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        // echo "World";
        $array = explode("?",$_POST['record_id']);
        // Util::dd($array);
        $employee_id = $array[0];
        $result = $di->get('employee')->delete($employee_id);
        // $result = DELETE_ERROR;
        switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, 'There was problem while deleting record, please try again later!');
                Util::redirect('manage-employee.php');
                break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, 'The record have been deleted successfully!');
                // Util::dd();
                Util::redirect('manage-employee.php');
                break;
        }
    }
}





if(isset($_POST['page']) && $_POST['page'] == 'manage_employee')
{
    // Util::dd($_POST);



    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    $di->get("employee")->getJSONDataForDataTable($draw, $search_parameter, $order_by, $start, $length);
}

if(isset($_POST['fetch']) && $_POST['fetch'] == 'employee'){
    $employee_id = $_POST['employee_id'];
    $result = $di->get('employee')->getEmployeeById($employee_id, PDO::FETCH_ASSOC);
    
    echo json_encode($result);
}

if(isset($_POST['fetch']) && $_POST['fetch'] == 'view-employee'){
    $employee_id = $_POST['employee_id'];
    $address = $di->get('address')->getAddressById($_POST['employee_id'], PDO::FETCH_ASSOC);
    $phone_no = $di->get('employee')->getPhoneNumber($_POST['employee_id']);
    $phone_whatsapp = $di->get('employee')->getPhoneNumber($_POST['employee_id'], true);
    $other_details = $di->get('employee')->getEmployeeById($_POST['employee_id'], PDO::FETCH_ASSOC);
    $result['address'] = $address;
    $result['other_details'] = $other_details;
    $result['phone_no'] = $phone_no;
    $result['phone_whatsapp'] = $phone_whatsapp;
    $result['email_id'] = $di->get('employee')->getEmail($_POST['employee_id'], PDO::FETCH_ASSOC);
    echo json_encode($result);
}

if(isset($_POST['fetch']) && $_POST['fetch'] == 'edit-employee'){
    $employee_id = $_POST['employee_id'];
    $result = $di->get('employee')->getEmployeeWithAddress($employee_id, false, PDO::FETCH_ASSOC);
    echo json_encode($result);
}

if(isset($_POST['delete']) && $_POST['which'] == 'phone_no'){
    $employee_id = $_POST['employee_id'];
    $phone_no = $_POST['record_id'];
    $result = $di->get('employee')->deleteNumber($employee_id, $phone_no, false);
    switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, 'There was problem while deleting record, please try again later!');
                Util::redirect('edit-employee.php?id='+$employee_id);
                break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, 'The record have been deleted successfully!');
                // Util::dd();
                Util::redirect('edit-employee.php?id='+$employee_id);
                break;
        }
}
if(isset($_POST['delete']) && $_POST['which'] == 'whatsapp'){
    $employee_id = $_POST['employee_id'];
    $phone_no = $_POST['record_id'];
    $result = $di->get('employee')->deleteNumber($employee_id, $phone_no, true);
    switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, 'There was problem while deleting record, please try again later!');
                Util::redirect('edit-employee.php?id='+$employee_id);
                break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, 'The record have been deleted successfully!');
                // Util::dd();
                Util::redirect('edit-employee.php?id='+$employee_id);
                break;
        }
}
if(isset($_POST['delete']) && $_POST['which'] == 'email'){
    $employee_id = $_POST['employee_id'];
    $phone_no = $_POST['record_id'];
    $result = $di->get('employee')->deleteEmail($employee_id, $phone_no, true);
    switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, 'There was problem while deleting record, please try again later!');
                Util::redirect('edit-employee.php?id='+$employee_id);
                break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, 'The record have been deleted successfully!');
                // Util::dd();
                Util::redirect('edit-employee.php?id='+$employee_id);
                break;
        }
}


if(isset($_POST['edit_employee'])){
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('employee')->update($_POST, $_POST['employee_id']);
        // Util::dd($result);
        switch($result)
        {
            case EDIT_ERROR:
                Session::setSession(EDIT_ERROR, 'There was problem while editing record, please try again later!');
                Util::redirect('manage-employee.php');
                break;
            case EDIT_SUCCESS:
                Session::setSession(EDIT_SUCCESS, 'The record have been updated successfully!');
                // Util::dd();
                Util::redirect('manage-employee.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession(VALIDATION_ERROR, 'There was some problem in validating your data at server side!');
                Session::setSession('errors',serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('manage-employee.php');
                break;
        }
    }
}











if(isset($_POST['add_employee']))
{
    //USER HAS REQUESTED TO ADD A NEW CATEGORY
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        // Util::dd($_POST);
        $result = $di->get('employee')->addEmployee($_POST);
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('add-employee.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-employee.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession('errors',serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-employee.php');
                break;
        }
    }
}

