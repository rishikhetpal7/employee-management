<?php

$app = __DIR__;

require_once "$app/../classes/helper_classes/Session.php";
require_once "$app/../classes/helper_classes/DependencyInjector.php";
require_once "$app/../classes/helper_classes/Config.php";
require_once "$app/../classes/helper_classes/Database.php";
require_once "$app/../classes/helper_classes/ErrorHandling.php";
require_once "$app/../classes/helper_classes/Hash.php";
require_once "$app/../classes/helper_classes/Validator.php";
require_once "$app/../classes/helper_classes/TokenHandler.php";
require_once "$app/../classes/helper_classes/Util.php";
// require_once "$app/../classes/helper_classes/MailConfigHelper.php";


require_once "$app/..//classes/Employee.php";
require_once "$app/..//classes/Address.php";
