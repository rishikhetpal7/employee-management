var Employee = function(){
    var Data = function(){
        var baseURL = window.location.origin;
        var filePath = "/helper/routing.php";
        var search = window.location.search.substr(1, window.location.search.length);
        var arr = search.split("=");
        $.ajax({
            url: baseURL + filePath,
            method: "POST",
            data: {
                "employee_id": `${arr[1]}`,
                "fetch": "view-employee"
            },
            dataType: "json",
            success: function(data){
                assignData(data);
            }
        });
        
    }

    return{
        getData: function(){
            Data();
        }
    }
}();
function assignData(data){
    autoAssigner(data['other_details'][0])
    console.log(data);
    if(data['phone_whatsapp'].length > 1){
        for(i = 2; i <= data['phone_whatsapp'].length; i++){
            addWhatsapp();
        }
    }
    // console.log(data['phone_no'].length > 1);
    if(data['phone_no'].length > 1){
        for(i = 2; i <= data['phone_no'].length; i++){
            addPhone();
        }
    }
    if(data['email_id'].length > 1){
        for(i = 2; i <= data['email_id'].length; i++){
            addEmail();
        }
    }
    if(data['address'].length > 1){
        addAddress()
    }
    detailsAssigner(data['phone_no'], 'phone_no')
    detailsAssigner(data['email_id'], 'email')
    detailsAssigner(data['phone_whatsapp'], 'phone_no_whatsapp')
    addressAssigner(data['address'])
}
function addressAssigner(data){
    insert = 0;
    from = 0;
    for(i = 0; i < data.length; i++){
        if(data[i]['primary_add'] == 1){
            for (var key of Object.keys(data[i])) {
                if(key == "address_id"){
                    $("#address_d_" + insert).val(data[i][key]);
                    continue
                }
                from = i
                console.log("#"+key+" " + insert + " => " +data[i][key])
                $("#"+key+"_" + insert).val(data[i][key]);
                
            }
            insert++
            break;
        }
    }
    if(data.length  == 1)
        return
    if(from == 0){
        from = 1
    }else{
        from = 0
    }
    for (var key of Object.keys(data[0])) {
        if(key == "address_id"){
            $("#address_d_" + insert).val(data[from][key]);
            continue
        }
        $("#"+key+"_" + insert).val(data[from][key]);
    }
}
function autoAssigner(data){
    for (var key of Object.keys(data)) {
        $("#"+key).val(data[key]);
    }
}
function detailsAssigner(data, string){
    insert = 2
    sub = ''
    hiddentxt =''
    hiddenValue =''
    if(string == 'phone_no'){
        source=string
        destination = string
        sub = 'no'
        hiddentxt = "phone_no_d"
        hiddenValue = "phone_no_id"
    }
    else if(string == 'phone_no_whatsapp'){ 
        source = 'phone_no'
        destination = string
        sub = 'no'
        hiddentxt = "phone_no_whatsapp_d"
        hiddenValue = "phone_whatsapp_id"
    }
    else if(string == 'email'){ 
        source = string
        destination = "email_id"
        sub = 'mail'
        hiddentxt = "email_id_d"
        hiddenValue = "email_id"
    }else{
        return
    }
    for(i = 1; i <= data.length; i++){
        // console.log("i " + (i-1))
        if(data[i-1]['primary_'+sub] == 1){
            // console.log("#"+destination)
            $("#"+destination).val(data[i-1][source])
            console.log("#"+hiddentxt+"_"+1 + " => " +data[i-1][hiddenValue])
            $("#"+hiddentxt+"_"+1).val(data[i-1][hiddenValue])
            break
        }
        
    }
    for(i = 1; i <= data.length; i++){
        // console.log("i " + (i-1))
        if(data[i-1]['primary_'+sub] == 0){
            // console.log("#"+destination+"_"+i)
            $("#"+destination+"_"+insert).val(data[i-1][source])
            $("#"+hiddentxt+"_"+insert).val(data[i-1][hiddenValue])
            insert++;
        }
        
    }
}
jQuery(document).ready(function(){
    Employee.getData();

    
});
whatsapp = 2
email = 2
address = 1
phone = 2
function addPhone(){
    $("#phone").append(`<div id="phone_${phone}"> 
    <label for="phone_no">Employee Phone Number</label><input type="hidden" id="phone_no_d_${phone}" name="phone_no_d_${phone}" value="1">
        <input type="number" name="phone_no_${phone}" id="phone_no_${phone}" class="form-control <?= $errors!='' && $errors->has('phone_no')? 'error': '';?>"
        placeholder="Enter Your Phone Number"/>
        <br> 
    </br>
    </div>`);
    phone++;
}
function addWhatsapp(){
    $("#whatsapp").append(`<div id="phone_wp_${whatsapp}"> 
    <label for="phone_no_whatsapp">Employee Whatsapp Number</label><input type="hidden" id="phone_no_whatsapp_d_${whatsapp}" name="phone_no_whatsapp_d_${whatsapp}">
                            <input type="number" name="phone_no_whatsapp_${whatsapp}" id="phone_no_whatsapp_${whatsapp}" class="form-control <?= $errors!='' && $errors->has('phone_no_whatsapp')? 'error': '';?>"
                            placeholder="Enter Your Whatsapp Number"/>
                            <br>
                            </br>
                            </div>`);
    whatsapp++;
}

function addEmail(){
    console.log(100000)
    $("#email").append(`<div id="email_${email}"> 
    <label for="email_id">Employee Email ID</label><input type="hidden" id="email_id_d_${email}" name="email_id_d_${email}">
                          <input type="email" name="email_id_${email}" id="email_id_${email}" class="form-control"
                          placeholder="Enter Your Email Id"/>
                          
                          <br>
                          </br>
                            </div>`);
}
function addAddress(){
    
    $("#address").append(`<br><br><div class="row">
    <div class="col-md-6"><input type="hidden" id="address_d_1" name="address_d_1">
    <input type="text" name="block_no_${address}" id="block_no_${address}" class="form-control <?= $errors!='' && $errors->has('block_no_${address}')? 'error': '';?>"
    placeholder="Enter Block No."/>
   
    </div>
    <div class="col-md-6">
    <input type="text" name="street_${address}" id="street_${address}" class="form-control <?= $errors!='' && $errors->has('street_${address}')? 'error': '';?>"
    placeholder="Enter street_${address}"/>
   

    </div>
    </div>

    <div class="row pt-3">
          <div class="col-md-4">
          <input type="text" name="city_${address}" id="city_${address}" class="form-control <?= $errors!='' && $errors->has('city_${address}')? 'error': '';?>"
          placeholder="Enter City"/>
      
          </div> 

          <div class="col-md-4">
          <input type="text" name="state_${address}" id="state_${address}" class="form-control <?= $errors!='' && $errors->has('state_${address}')? 'error': '';?>"
          placeholder="Enter State"/>
        
          </div>

          <div class="col-md-4">
          <input type="text" name="town_${address}" id="town_${address}" class="form-control <?= $errors!='' && $errors->has('town_${address}')? 'error': '';?>"
          placeholder="Enter Town"/>
        
     </div>

     <div class="row pt-3 pb-3">
          <div class="col-md-6">
          <input type="number" name="pincode_${address}" id="pincode_${address}" class="form-control <?= $errors!='' && $errors->has('pincode_${address}')? 'error': '';?>"
          placeholder="Enter Pin Code"/>
          </div> 

          <div class="col-md-6">
          <input type="text" name="country_${address}" id="country_${address}" class="form-control <?= $errors!='' && $errors->has('country_${address}')? 'error': '';?>"
          placeholder="Enter Country"/>
        
     </div>`);
}

