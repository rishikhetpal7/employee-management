var TableDataTables = function(){
    var handleCategoryTable = function(){
        var manageCategoryTable = $("#manage-employee-datatable");
        var baseURL = window.location.origin;
        var filePath = "/helper/routing.php";
        var oTable = manageCategoryTable.dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "page": "manage_employee"
                }
            },
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
            ],
            "order": [
                [0, "ASC"]
            ],
            "columnDefs": [{
                'orderable': false,
                'targets': [-2, -1]
            }],
        });

        manageCategoryTable.on('click', '.view', function(){
            id = $(this).attr('id');
            window.location.href = baseURL + `/views/pages/view-employee.php?id=${id}`;
        });

        manageCategoryTable.on('click', '.edit', function(){
            id = $(this).attr('id');
            window.location.href = baseURL + `/views/pages/edit-employee.php?id=${id}`;
        });

        manageCategoryTable.on('click', '.delete', function(){
            id = $(this).attr('id');
            $("#record_id").val(id);
            // $.ajax({
            //     url: baseURL + filePath,
            //     method: "POST",
            //     data: {
            //         "employee_id": id,
            //         "fetch": "employee"
            //     },
            //     dataType: "json"
            // });
        });
    }

    return{
        init: function(){
            handleCategoryTable();
        }
    }
}();


jQuery(document).ready(function(){
    TableDataTables.init();

    
});

