$(function(){
    $("#add-employee").validate({
        'rules': {
            'first_name':{
                required: true,
                minlength: 2,
                maxlength: 255
            },
            'last_name':{
                required:true,
                minlength:2,
                maxlength:255
            },
            'gst_no':{
                required:true
            },
            'phone_no':{
                required:true
            },
            'email_id':{
                required: true,
                email: true
            },
            'gender':{
                required:true
            }
        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        }
    });
});
var phone = 2
var id = 1
function addPhone(){
    $("#phone").append(`<div id="phone_${phone}"> 
    <label for="phone_no">Employee Phone Number</label>
        <input type="number" name="phone_no_${phone}" id="phone_no_${phone}" class="form-control <?= $errors!='' && $errors->has('phone_no')? 'error': '';?>"
        placeholder="Enter Your Phone Number"/><button type="button" class="btn btn-outline-danger" onclick="deletePhone(${phone})">delete</button>
        <br> 
    </br>
    </div>`);
    phone++;
}
function deletePhone(id){

  $("#phone_"+id).remove();
}
function deleteWhatsapp(id){

  $("#phone_wp_"+id).remove();
}
function deleteEmail(id){

  $("#email_"+id).remove();
}
whatsapp =2;
function addWhatsapp(){
    $("#whatsapp").append(`<div id="phone_wp_${whatsapp}"> 
    <label for="phone_no_whatsapp">Employee Whatsapp Number</label>
                            <input type="number" name="phone_no_whatsapp_${whatsapp}" id="phone_no_whatsapp_${whatsapp}" class="form-control <?= $errors!='' && $errors->has('phone_no_whatsapp')? 'error': '';?>"
                            placeholder="Enter Your Whatsapp Number"/><button type="button" class="btn btn-outline-danger" onclick="deleteWhatsapp(${whatsapp})">delete</button>
                            <br>
                            </br>
                            </div>`);
    whatsapp++;
}
email = 2;
function addEmail(){
    $("#email").append(`<div id="email_${email}"> 
    <label for="email_id">Employee Email ID</label>
                          <input type="email" name="email_id_${email}" id="email_id_${email}" class="form-control <?= $errors!='' && $errors->has('email_id')? 'error': '';?>"
                          placeholder="Enter Your Email Id"/><button type="button" class="btn btn-outline-danger" onclick="deleteEmail(${email})">delete</button>
                          
                          <br>
                          </br>
                            </div>`);
    email++;
}
function addAddress(){
    if(id > 1){
        return
    }
    $("#address").append(`<br><br><div class="row">
    <div class="col-md-6">
    <input type="text" name="block_no_${id}" id="block_no_${id}" class="form-control <?= $errors!='' && $errors->has('block_no_${id}')? 'error': '';?>"
    placeholder="Enter Block No."/>
   
    </div>
    <div class="col-md-6">
    <input type="text" name="street_${id}" id="street_${id}" class="form-control <?= $errors!='' && $errors->has('street_${id}')? 'error': '';?>"
    placeholder="Enter street_${id}"/>
   

    </div>
    </div>

    <div class="row pt-3">
          <div class="col-md-4">
          <input type="text" name="city_${id}" id="city_${id}" class="form-control <?= $errors!='' && $errors->has('city_${id}')? 'error': '';?>"
          placeholder="Enter City"/>
      
          </div> 

          <div class="col-md-4">
          <input type="text" name="state_${id}" id="state_${id}" class="form-control <?= $errors!='' && $errors->has('state_${id}')? 'error': '';?>"
          placeholder="Enter State"/>
        
          </div>

          <div class="col-md-4">
          <input type="text" name="town_${id}" id="town_${id}" class="form-control <?= $errors!='' && $errors->has('town_${id}')? 'error': '';?>"
          placeholder="Enter Town"/>
        
     </div>

     <div class="row pt-3 pb-3">
          <div class="col-md-6">
          <input type="number" name="pincode_${id}" id="pincode_${id}" class="form-control <?= $errors!='' && $errors->has('pincode_${id}')? 'error': '';?>"
          placeholder="Enter Pin Code"/>
          </div> 

          <div class="col-md-6">
          <input type="text" name="country_${id}" id="country_${id}" class="form-control <?= $errors!='' && $errors->has('country_${id}')? 'error': '';?>"
          placeholder="Enter Country"/>
        
     </div>`);
    $("#number_of_address").val(id+1);
    id++;
}
